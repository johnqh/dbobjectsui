//
//  LinkedRoutingCollectionInteractor.m
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "LinkedDbCollectionLoader.h"
#import "ObjectInteractor.h"
#import "DbObject.h"
#import "DbCollection.h"
#import "ApiParamsProtocol.h"
#import "WebService.h"
#import "JsonLoader.h"
#import "DictionaryUtils.h"

@implementation LinkedDbCollectionLoader

@synthesize parentInteractor = _parentInteractor;
@synthesize parent = _parent;
@synthesize collectionName = _collectionName;

- (NSDictionary *)additionalApiParams
{
    NSMutableDictionary * params = self.params ? [NSMutableDictionary dictionaryWithDictionary:self.params] : [NSMutableDictionary dictionary];
    if ([self.parent conformsToProtocol:@protocol(ApiParamsProtocol)])
    {
        id<ApiParamsProtocol> apiParams = (id<ApiParamsProtocol>)self.parent;
        NSDictionary * moreParams = [apiParams apiParamsForProperty:self.collectionName];
        [params addEntriesFromDictionary:moreParams];
    }
    return params;
}

- (NSObject *)apiReference
{
    return self.parent;
}

- (void)loadFromLocal
{
    if (self.parent)
    {
        if (self.collectionName)
        {
            DbObject * obj = (DbObject *)self.parent;
            SEL selector = NSSelectorFromString(self.collectionName);
            if (selector)
            {
                IMP imp = [obj methodForSelector:selector];
                id (*func)(id, SEL) = (void *)imp;
                id value = func(obj, selector);

                if ([value isKindOfClass:[DbCollection class]])
                {
                    self.collection = value;
                }
                else
                {
                    if (value)
                    {
                        NSLog(@"%@ is not a collection", self.collectionName);
                    }
                    else
                    {
                        NSLog(@"%@ does not exist", self.collectionName);
                    }
                }

            }
            else
            {
                NSLog(@"%@ does not exist", self.collectionName);
            }
        }
        else
        {
            NSLog(@"Collection was not set for %@", NSStringFromClass([self.parent class]));
        }
    }
    else
    {
        self.collection = nil;
    }
}

- (void)setParentInteractor:(ObjectInteractor *)parentInteractor
{
    if (_parentInteractor != parentInteractor)
    {
        if (_parentInteractor)
        {
            [_parentInteractor removeObserver:self forKeyPath:@"entity"];
        }
        _parentInteractor = parentInteractor;
        if (_parentInteractor)
        {
            [_parentInteractor addObserver:self forKeyPath:@"entity" options:NSKeyValueObservingOptionInitial context:nil];
        }
    }
}

- (void)setParent:(NSObject<ModelObject> *)parent
{
    if (_parent != parent)
    {
        _parent = parent;
        [self load];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (object == _parentInteractor)
    {
        if ([keyPath isEqualToString:@"entity"])
        {
            self.parent = _parentInteractor.entity;
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc
{
    self.parentInteractor = nil;
}
@end
