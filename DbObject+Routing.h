//
//  DbObject+Routing.h
//  DbObjectsUI
//
//  Created by Qiang Huang on 12/17/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "DbObject.h"
#import "RoutingIdentifierProtocol.h"

@interface DbObject (Routing)<RoutingIdentifierProtocol>

@end
