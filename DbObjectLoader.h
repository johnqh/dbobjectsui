//
//  RoutingObjectInteractor.h
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectLoaderProtocol.h"
#import "ModelObject.h"

@interface DbObjectLoader : NSObject<ObjectLoaderProtocol>

@property (nonatomic, strong) IBInspectable NSString * databaseTag;
@property (nonatomic, strong) IBInspectable NSString * objectClassName;
@property (nonatomic, strong) IBInspectable NSString * apiClassName;
@property (nonatomic, strong) IBInspectable NSString * apiPath;
@property (nonatomic, strong) IBInspectable NSString * path;
@property (nonatomic, strong) NSObject<ModelObject> * entity;
@property (nonatomic, assign) BOOL loadFromStorage;
@property (nonatomic, strong) NSDictionary * params;

@end
