//
//  DbObject+Routing.m
//  DbObjectsUI
//
//  Created by Qiang Huang on 12/17/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "DbObject+Routing.h"
#import "DbTable.h"
#import "DbField.h"

@implementation DbObject (Routing)

- (NSDictionary *)identifier
{
    if (self.keyField)
    {
        return @{self.keyField : self.keyValue};
    }
    return nil;
}
@end
