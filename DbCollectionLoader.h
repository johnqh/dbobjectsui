//
//  RoutingCollectionInteractor.h
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionLoaderProtocol.h"
#import "ModelObject.h"

@interface DbCollectionLoader : NSObject<CollectionLoaderProtocol>

@property (nonatomic, strong) IBInspectable NSString * objectClassName;
@property (nonatomic, strong) IBInspectable NSString * additionalParamsString;
@property (nonatomic, strong) IBInspectable NSString * apiClassName;
@property (nonatomic, strong) IBInspectable NSString * apiPath;
@property (nonatomic, strong) IBInspectable NSString * path;
@property (nonatomic, strong) IBInspectable NSString * sort;

@property (nonatomic, strong) NSDictionary * params;
@property (nonatomic, strong) NSObject<ModelList> * collection;

@property (nonatomic, readonly) NSObject * apiReference;

- (void)loadFromLocal;
- (void)loadFromApi;

@end
