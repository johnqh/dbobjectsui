//
//  LinkedRoutingCollectionInteractor.h
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//
#import "DbCollectionLoader.h"

@class ObjectInteractor;

@interface LinkedDbCollectionLoader : DbCollectionLoader

@property (nonatomic, strong) IBOutlet ObjectInteractor * parentInteractor;
@property (nonatomic, strong) NSObject<ModelObject> * parent;

@property (nonatomic, strong) IBInspectable NSString * collectionName;


@end
