//
//  RoutingCollectionInteractor.m
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "RootDbCollectionLoader.h"
#import "DbObject.h"
#import "DbCollection.h"
#import "DbDatabases.h"
#import "RoutingRequest.h"
#import "WebService.h"
#import "DictionaryUtils.h"

@implementation RootDbCollectionLoader

@synthesize databaseTag = _databaseTag;
@synthesize loadFromStorage = _loadFromStorage;

- (void)loadFromLocal
{
    if (self.objectClassName)
    {
        Class class = NSClassFromString(self.objectClassName);
        if (class)
        {
            if ([class isSubclassOfClass:[DbObject class]])
            {
                DbDatabase * db = _databaseTag ? [[DbDatabases databases] database:_databaseTag] : [DbDatabases singleton];
                DbObject * example = [[class alloc] initWithDb:db];
                for (NSString * key in self.params)
                {
                    NSString * value = [DictionaryUtils asString:self.params[key]];
                    [example setPrivate:key data:value];
                }
                DbCollection * collection = [DbCollection collectionWithExample:example];

                if (self.sort)
                {
                    NSArray * sorts = [self.sort componentsSeparatedByString:@","];
                    for (NSString * sort in sorts)
                    {
                        [collection addSorting:[DbSortDescriptor sortOnKey:sort ascending:true]];
                    }
                }
                [collection loadFromDb];
                self.collection = collection;
            }
            else
            {
                NSLog(@"%@ is not a subclass of DbObject", self.objectClassName);
            }
        }
        else
        {
            NSLog(@"%@ does not exist", self.objectClassName);
        }
    }
}

@end
