//
//  LinkedDbObjectLoader.m
//  DbObjects
//
//  Created by Qiang Huang on 12/10/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "LinkedDbObjectLoader.h"
#import "ObjectInteractor.h"
#import "DbObject.h"
#import "DbCollection.h"
#import "ApiParamsProtocol.h"
#import "WebService.h"

@implementation LinkedDbObjectLoader

- (void)load
{
    if (self.parent)
    {
        if (self.objectName)
        {
            DbObject * obj = (DbObject *)self.parent;
            SEL selector = NSSelectorFromString(self.objectName);
            if (selector)
            {
                IMP imp = [obj methodForSelector:selector];
                id (*func)(id, SEL) = (void *)imp;
                id value = func(obj, selector);

                if ([value isKindOfClass:[DbObject class]])
                {
                    self.entity = value;

                    if (self.apiPath && self.apiClassName)
                    {
                        if ([self.parent conformsToProtocol:@protocol(ApiParamsProtocol)])
                        {
                            id<ApiParamsProtocol> apiParams = (id<ApiParamsProtocol>)self.parent;
                            NSDictionary * params = [apiParams apiParamsForProperty:self.objectName];

                            Class class = NSClassFromString(self.apiClassName);
                            if (class)
                            {
                                if ([class isSubclassOfClass:[WebService class]])
                                {
                                    WebService * webService = (WebService *)[[class alloc] init];
                                    [webService run:self action:self.apiPath params:params class:NSClassFromString(self.objectClassName) reference:self.parent];
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (value)
                    {
                        NSLog(@"%@ is not a collection", self.objectName);
                    }
                    else
                    {
                        NSLog(@"%@ does not exist", self.objectName);
                    }
                }

            }
            else
            {
                NSLog(@"%@ does not exist", self.objectName);
            }
        }
        else
        {
            NSLog(@"Object was not set for %@", NSStringFromClass([self.parent class]));
        }
    }
    else
    {
        self.entity = nil;
    }
}

- (void)setParentInteractor:(ObjectInteractor *)parentInteractor
{
    if (_parentInteractor != parentInteractor)
    {
        if (_parentInteractor)
        {
            [_parentInteractor removeObserver:self forKeyPath:@"entity"];
        }
        _parentInteractor = parentInteractor;
        if (_parentInteractor)
        {
            [_parentInteractor addObserver:self forKeyPath:@"entity" options:NSKeyValueObservingOptionInitial context:nil];
        }
    }
}

- (void)setParent:(NSObject<ModelObject> *)parent
{
    if (_parent != parent)
    {
        self.entity = nil;
        
        _parent = parent;
        [self load];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (object == _parentInteractor)
    {
        if ([keyPath isEqualToString:@"entity"])
        {
            self.parent = _parentInteractor.entity;
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)dealloc
{
    self.parentInteractor = nil;
}
@end
