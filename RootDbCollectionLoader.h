//
//  RoutingCollectionInteractor.h
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "DbCollectionLoader.h"

@interface RootDbCollectionLoader : DbCollectionLoader

@property (nonatomic, strong) IBInspectable NSString * databaseTag;
@property (nonatomic, assign) IBInspectable BOOL loadFromStorage;

@end
