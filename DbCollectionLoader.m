//
//  RoutingCollectionInteractor.m
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "DbCollectionLoader.h"
#import "DbObject.h"
#import "DbCollection.h"
#import "DbDatabases.h"
#import "RoutingRequest.h"
#import "WebService.h"
#import "DictionaryUtils.h"
#import "JsonLoader.h"

@implementation DbCollectionLoader

- (void)setAdditionalParamsString:(NSString *)additionalParamsString
{
    _additionalParamsString = additionalParamsString;
    self.params = additionalParamsString ? [DictionaryUtils asDictionary:[JsonLoader loadFromJsonString:_additionalParamsString]] : nil;
}

- (NSDictionary *)additionalApiParams
{
    return self.params;
}

- (NSObject *)apiReference
{
    return nil;
}

- (void)load
{
    [self loadFromLocal];
    [self loadFromApi];
}

- (void)loadFromLocal
{

}

- (void)loadFromApi
{
    if (self.apiPath && self.apiClassName)
    {
        Class class = NSClassFromString(self.apiClassName);
        if (class)
        {
            if ([class isSubclassOfClass:[WebService class]])
            {
                WebService * webService = (WebService *)[[class alloc] init];
                [webService run:self action:self.apiPath params:self.additionalApiParams class:NSClassFromString(self.objectClassName) reference:self.apiReference];
            }
        }
    }
}
@end
