//
//  RoutingObjectInteractor.m
//  DbObjects
//
//  Created by Qiang Huang on 11/26/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import "DbObjectLoader.h"
#import "DbObject.h"
#import "DbDatabases.h"
#import "WebService.h"
#import "RoutingRequest.h"
#import "DictionaryUtils.h"

@implementation DbObjectLoader

- (void)load
{
    if (self.objectClassName)
    {
        Class class = NSClassFromString(self.objectClassName);
        if (class)
        {
            if ([class isSubclassOfClass:[DbObject class]])
            {
                DbDatabase * db = _databaseTag ? [[DbDatabases databases] database:_databaseTag] : [DbDatabases singleton];
                DbObject * example = [[class alloc] initWithDb:db];
                for (NSString * key in self.params)
                {
                    NSString * value = [DictionaryUtils asString:self.params[key]];
                    [example setPrivate:key data:value];
                }
                self.entity = [example loadFromDb];

                if (self.apiPath && self.apiClassName)
                {
                    Class class = NSClassFromString(self.apiClassName);
                    if (class)
                    {
                        if ([class isSubclassOfClass:[WebService class]])
                        {
                            WebService * webService = (WebService *)[[class alloc] init];
                            [webService run:self action:self.apiPath params:self.params class:NSClassFromString(self.objectClassName) reference:nil];
                        }
                    }
                }
            }
            else
            {
                NSLog(@"%@ is not a subclass of DbObject", self.objectClassName);
            }
        }
        else
        {
            NSLog(@"%@ does not exist", self.objectClassName);
        }
    }
}
@end
