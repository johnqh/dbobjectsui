//
//  LinkedDbObjectLoader.h
//  DbObjects
//
//  Created by Qiang Huang on 12/10/16.
//  Copyright © 2016 Sudobility. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjectLoaderProtocol.h"
#import "ModelObject.h"

@class ObjectInteractor;

@interface LinkedDbObjectLoader : NSObject<ObjectLoaderProtocol>

@property (nonatomic, strong) IBOutlet ObjectInteractor * parentInteractor;
@property (nonatomic, strong) NSObject<ModelObject> * parent;

@property (nonatomic, strong) IBInspectable NSString * objectName;

@property (nonatomic, strong) IBInspectable NSString * objectClassName;
@property (nonatomic, strong) IBInspectable NSString * apiClassName;
@property (nonatomic, strong) IBInspectable NSString * apiPath;
@property (nonatomic, strong) IBInspectable NSString * path;
@property (nonatomic, strong) NSObject<ModelList> * entity;

@end
